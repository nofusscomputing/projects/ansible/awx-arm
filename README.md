# Automagic AWX Builds for arm64

![Dynamic JSON Badge](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fnofusscomputing%2Fprojects%2Fansible%2Fawx-arm%2F-%2Fraw%2Fmaster%2F.metadata.json&query=%24.project_status&style=plastic&logo=gitlab&label=Project%20Status&color=green)


This repository exists to automate AWX container image builds that are inclusive of arm64 images. Why? no official arm images for awx.

Although there are offical builds for amd64, this repo will build both architectures. This provides a single image reference to use accross a cluster that has multiple archtecture nodes.


CI Jobs and logs are open for you to view should you wish.


This repository has been setup on a schedule for the wizardry to be conducted to build and publish new releases when they come out.


Images are published to dockerhub [`nofusscomputing/awx:{tag}`](https://hub.docker.com/r/nofusscomputing/awx)

Want to see Official builds for ARM64, head on over to [github #14643](https://github.com/ansible/awx/issues/14643) and let the AWX team know.


# Workflow

The basic workflow for this repository is as follows:

- **Check Task:**

    - Check for the latest tag [@awx home](https://github.com/ansible/awx)

    - if newer tag found, 
      
        - pull the code

        - apply the patch for arm builds to work

            - on success trigger build

            - on failure repo owner notified to fix the patch

    - if no tag found, do nothing

- **Build Task:**

    - Only occurs when git tag created in this repo. _**NOTE:** git tag must match the AWX release tag._

    - Same tasks as check

    - build amd64 and arm64 architectures container images

    - push to container registry


# Dev notes

1. Start the container

    ``` bash

    docker run \
      --rm \
      -ti \
      -e ANSIBLE_HOST_KEY_CHECKING=false \
      -v ~/aa-shits-and-gigs/awx-arm:/workdir \
      -w /workdir \
      --net=host \
      nofusscomputing/ansible-ee:dev \
      bash

    ```

1. run the playbook `ansible-playbook automagic.yaml --extra-vars "tag_latest_github={tag_to_clone}" -vvv --tags dev`

1. make the appropriate edits manually

1. make a patch `./make-patch.sh`

1. commit the changes.

1. tag the commit with the version to build

1. push all changes and tags `git push --tags`

If for some reason you have to manually apply the patch use `cd awx; git apply ../diff.patch; cd ..`

If the build is successful and the dockerhub push fails launch a job on `master` branch with variables `NO_BUILD=1` and `CI_COMMIT_TAG={tag to push}`

# Licence

These containers share the AWX Licence which can be viewed at [it's home](https://github.com/ansible/awx/blob/devel/LICENSE.md)

Commits in this repository are MIT licenced.